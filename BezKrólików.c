#include <stdio.h>

int main() {
    int i, j, n;
    scanf("%d", &n);
    for (i = 0; i < n; i++) {
        int x;
        scanf("%d", &x);
        int tab[x + 1];
        tab[0] = 4;
        tab[1] = 7;
        for (j = 2; j <= x; j++) {
            tab[j] = ((2 * tab[j - 1]) + (5 * tab[j - 2])) % 2011;
        }
        int result = tab[x];
        printf("%d\n", result);
    }
    return 0;
}
#include <stdio.h>

int main() {
    int n;
    scanf("%d", &n);
    int i, j;
    int m[n];
    for (i = 0; i < n; ++i) {
        scanf("%d", &m[i]);
    }
    for (i = 0; i < n; ++i) {
        printf("%c", 'B');
        for (j = 0; j < m[i]; ++j) {
            printf("%c", 'I');
        }
        printf("%s", "G B");
        for (j = 0; j < m[i]; ++j) {
            printf("%c", 'O');
        }
        printf("%c", 'M');
        for (j = 0; j < m[i]; ++j) {
            printf("%c", '!');
        }
        printf("%s", "\n");
    }
    return 0;
}
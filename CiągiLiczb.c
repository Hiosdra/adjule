int main()
{
    int x, y;
    char p;
    scanf("%d %d", &x, &y);
    scanf(" %c", &p);
    bool shouldBeEven;
    if (p == 'p') {
        shouldBeEven = true;
    } else { //todo throw for non p or n
        shouldBeEven = false;
    }
    int i;
    for (i = x; i <= y; i++) {
        if (shouldBeEven && i % 2 == 0) {
            printf("%d ", i);
        } else if(!shouldBeEven && i % 2 == 1) {
            printf("%d ", i);
        }
    }
    return 0;
}
#include <stdio.h>
#define DEBUG 0

int main() {
    int i, j, k, n, m;
    scanf("%d", &n);
    int a[n];
    for (i = 0; i < n; ++i) {
        scanf("%d", &a[i]);
    }
    scanf("%d", &m);
    int b[m];
    for (i = 0; i < m; ++i) {
        scanf("%d", &b[i]);
    }
#if DEBUG
    printf("%s\n", "3 part:");
#endif
    for (k = 1; k <= m; ++k) {
        for (i = 0; i < n; ++i) {
            int isIn = 0;
            for (j = 0; j < k; ++j) {
                if(a[i] == b[j]) {
                    ++isIn;
                    break;
                }
            }
            if(isIn == 0) {
                printf("%d ", a[i]);
            }
        }
        printf("%s", "\n");
    }
    return 0;
}



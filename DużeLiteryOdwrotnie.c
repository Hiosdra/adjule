#include <stdio.h>

int main() {
    int n;
    scanf("%d", &n);
    char letters[n];
    int i;
    for (i = 0; i < n; i++) {
        scanf(" %c", &letters[i]);
    }
    for (i = n-1; i >= 0; --i) {
        int letter = (int)letters[i];
        if(letter > 64 && letter < 91) {
            printf("%c ", letters[i]);
        }
    }
    return 0;
}
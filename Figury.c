#include <stdio.h>
#include <math.h>

int main() {
    int i, n;
    scanf("%d", &n);
    int edges[n][2];
    float circuit = 0.0;
    float sum = 0.0;
    for (i = 0; i < n; ++i) {
        scanf("%d %d", &edges[i][0], &edges[i][1]);
        sum += sqrt(edges[i][0] * edges[i][0] + edges[i][1] * edges[i][1]);
        if (i > 0) {
            circuit += sqrtf(powf(edges[i][0] - edges[i - 1][0], 2) + powf(edges[i][1] - edges[i - 1][1], 2));
        }
    }
    circuit += sqrtf(powf(edges[0][0] - edges[n - 1][0], 2) + powf(edges[0][1] - edges[n - 1][1], 2));
    printf("%.3f %.3f", circuit, sum);
    return 0;
}
#include <stdio.h>
#include <math.h>

struct Film {
    char title[100];
    int year;
    char author[100];
    int birthYear;
};

int main() {
    int i, n;
    scanf("%d", &n);
    struct Film films[n];
    for (i = 0; i < n; ++i) {
        scanf("%s %d %s %d", &films[i].title, &films[i].year, &films[i].author, &films[i].birthYear);
    }
    int year, age;
    char letter;
    scanf("%d\n %d\n %c", &year, &age, &letter);
    for (i = 0; i < n; ++i) {
        if (films[i].year > year) {
            printf("%s %d (%s)\n", films[i].title, films[i].year, films[i].author);
        }
    }
    for (i = 0; i < n; ++i) {
        if (2014 - films[i].birthYear < age) {
            printf("%s %d (%s)\n", films[i].title, films[i].year, films[i].author);
        }
    }
    for (i = 0; i < n; ++i) {
        if (films[i].title[0] == letter) {
            printf("%s %d (%s)\n", films[i].title, films[i].year, films[i].author);
        }
    }
    return 0;
}
#include <stdio.h>

#define DEBUG 0
#define DEBUG_TWO 0
#define DEBUG_THREE 0
#define DEBUG_FOUR 0

int main() {
    int i, n;
    scanf("%d", &n);
    char advanced[n][20];
    char basic[n][20];
    for (i = 0; i < n; ++i) {
        scanf("%s %s", &advanced[i], &basic[i]);
    }
#if DEBUG_THREE
    for (i = 0; i < n; ++i) {
        printf("--: $%s$ $%s$\n", advanced[i], basic[i]);
    }
#endif

    int m;
    scanf("%d", &m);
    char word[m][20];
    for (i = 0; i < m; ++i) {
        scanf("%s", &word[i]);
    }
#if DEBUG_FOUR
    for (i = 0; i < m; ++i) {
        printf("--W: $%s$\n", word[i]);
    }
#endif

    int j;
    for (i = 0; i < m; i++) {
        int nameIndex = -1;
        for (j = 0; j < n; ++j) {
#if DEBUG_TWO
            printf("--Advanced: %s; Word: %s; i: %d; j: %d\n", advanced[i], word[j], i, j);
#endif
            if (strcmp(advanced[j], word[i]) == 0) {
                nameIndex = j;
#if DEBUG
                printf("Name index: %d\n", nameIndex);
#endif
                break;
            }
        }
        if (nameIndex == -1) {
            printf("%s ", word[i]);
        } else {
            printf("%s ", basic[nameIndex]);
        }
    }
    return 0;
}



#include <stdio.h>
#include <stdlib.h>

int main() {
    char chars[100];
    scanf("%s", &chars);
    int i = 0;
    while (chars[i] != NULL) {
        ++i;
    }
    int j;
    for (j = i; j > 0; --j) {
        printf("%c", chars[j -1]);
    }
    return 0;
}

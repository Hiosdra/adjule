#include <stdio.h>

int main() {
    int n, i;
    scanf("%d", &n);
    for (i = 0; i < n; ++i) {
        int k, l;
        char operation;
        scanf("%d %c %d", &k, &operation, &l);
        if (operation == '+') {
            printf("%d", k + l);
        } else if (operation == '-') {
            printf("%d", k - l);
        } else {
            printf("%d", k * l);
        }
    }
    return 0;
}
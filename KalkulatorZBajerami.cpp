#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int isnum(char A[20]) {
    int T = 0;
    if (A[0] == 45) {
        for (int j = 1; j < strlen(A); j++) {
            if ((A[j] < 48) || (A[j] > 57)) {
                T = 1;
                break;
            }
        }
    } else {
        for (int j = 0; j < strlen(A); j++) {
            if ((A[j] > 47) && (A[j] < 58)) {
            } else {
                T = 1;
                break;
            }
        }
    }
    if (T == 0) {
        return 0;
    } else {
        return 1;
    }
}

int main() {
    int n, a, b, t = 0, x = 0;
    char A[20];
    char B[20];
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        t = 0;
        x = 0;
        scanf("%s %s", &A, &B);
        if (isnum(A) == 0) {
            a = atoi(A);
            t = t + 1;
        }
        if (isnum(B) == 0) {
            b = atoi(B);
            t = t + 1;
            x = x + 1;
        }
        if (t > 0) {
            if (x == 0) {
                printf("%c", B[a - 1]);
            }
            if (x == 1) {
                if (t == 2) {
                    printf("%d", a + b);
                } else {
                    printf("%c", A[b - 1]);
                }
            }
        } else if (t == 0) {
            strncat(A, B, 20);
            printf("%s", A);
        }
        printf("\n");
    }
}
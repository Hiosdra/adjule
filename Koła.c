#include <stdio.h>
#include <math.h>

struct Circle {
    int x;
    int y;
    int radius;
};

int main() {
    int i, n, m;
    scanf("%d", &n);
    struct Circle circles[n];
    for (i = 0; i < n; ++i) {
        scanf("%d %d %d", &circles[i].x, &circles[i].y, &circles[i].radius);
    }
    scanf("%d", &m);
    for (i = 0; i < m; ++i) {
        int a, b;
        scanf("%d %d", &a, &b);
        a -= 1;
        b -= 1;
        double eucl = sqrt(pow((circles[b].x - circles[a].x), 2) + pow((circles[b].y - circles[a].y), 2));
        double p1 = 3.14 * pow(circles[a].radius, 2);
        double p2 = 3.14 * pow(circles[b].radius, 2);
        printf("%.2lf %.2lf\n", eucl, (p1 > p2) ? p1 : p2);
    }
    return 0;
}
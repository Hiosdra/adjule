#include<stdio.h>

int main(){
    int n, i, j, x;
    scanf("%d", &x);
    for(i = 0; i < x; i++){
        scanf("%d", &n);
        if(n > 2){
            int T[n+1];
            T[0] = 1;
            T[1] = 1;
            T[2] = 1;
            for(j = 3; j < n + 1; j++){
                T[j] = T[j-1] + 2*T[j - 3];
            }
            printf("%d\n", T[n]);
        }
        else printf("%d\n", 1);
    }
    return 0;
}

#include <stdio.h>


int main() {
    int i, j, n, m;
    int dziel[200000];
    int liczRok[10][2];
    scanf("%d", &n);
    for (i = 0; i < n; i++) {
        scanf("%d", &dziel[i]);
    }
    scanf("%d", &m);
    for (i = 0; i < m; i++) {
        scanf("%d", &liczRok[i][0]);
        for (j = 0; j < n; j++) {
            if (liczRok[i][0] % dziel[j] == 0) {
                liczRok[i][1] = liczRok[i][1] + 1;
            }
        }
    }
    for (i = 0; i < m; i++) {
        printf("%d \n", liczRok[i][1]);
    }
    return 0;
}
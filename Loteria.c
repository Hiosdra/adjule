#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int compare(const void *a1, const void *a2);

int main() {
    int n, i;
    scanf("%d", &n);
    int numbers[n];
    for (i = 0; i < n; ++i) {
        scanf("%d", &numbers[i]);
    }
    qsort(&numbers, n, sizeof(numbers[0]), compare);
    for(i = 1; i < n; i++) {
        if((numbers[i] == numbers[i - 1]) && (i + 1 == n || numbers[i] != numbers[i + 1]))  {
            printf("%d\n", numbers[i]);
        }
    }
}

int compare(const void *a1, const void *a2) {
    int x1 = *(int*)a1;
    int x2 = *(int*)a2;
    if(x1 < x2) return -1;
    if(x1 == x2) return 0;
    if(x1 > x2) return 1;
}
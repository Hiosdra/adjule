#include <stdio.h>
#include <math.h>

int main() {
    int n, i, j;
    scanf("%d", &n);
    for (i = 0; i < n; ++i) {
        int numbers[4];
        scanf("%d %d %d %d", &numbers[0], &numbers[1], &numbers[2], &numbers[3]);
        for (j = numbers[0];; ++j) {
            if (j % numbers[0] == 0
                && j % numbers[1] == 0
                && j % numbers[2] == 0
                && j % numbers[3] == 0) {
                break;
            }
        }
        printf("%d\n", j);
    }
    return 0;
}
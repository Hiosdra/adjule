void swap(int *a, int *b) {
    int temp = (*a);
    (*a) = (*b);
    (*b) = temp;
}

int main() {
    int numOfOperations, a, b;
    char k;
    scanf("%d", &numOfOperations);
    int i;
    for (i = 0; i < numOfOperations; i = i + 1) {
        scanf("%d %d %c", &a, &b, &k);
        if (a != b) {
            if (a > b) {
                swap(&a, &b);
            }
            int j, result = 0;
            if (k == '+') {
                for (j = a; j <= b; j = j + 1) {
                    result += j;
                }
            } else if (k == '-') {
                for (j = a; j <= b; j = j + 1) {
                    result -= j;
                }
            } else if (k == '*') {
                result = 1;
                for (j = a; j <= b; j = j + 1) {
                    result *= j;
                }
            }
            printf("%d", result);
        }
    }
    return 0;
}
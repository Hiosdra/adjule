#include <stdio.h>

int main() {
    int n, m;
    scanf("%d %d", &n, &m);
    int tab[n][m];
    int i, j;
    for (i = 0; i < n; ++i) {
        for (j = 0; j < m; ++j) {
            scanf("%d", &tab[i][j]);
        }
    }
    for (i = 0; i < m; ++i) {
        for (j = n-1; j >= 0; --j) {
            printf("%d ", tab[j][i]);
        }
        printf("%s", "\n");
    }
    return 0;
}
#include <stdio.h>
#include <string.h>

int compare(char A[10], char B[10]);
void reverse(int i);

char TAB[10][30];
char BAT[10][30];


int main() {
    int n;
    scanf("%d", &n);

    for (int i = 0; i < n; i++) {
        scanf("%s", &TAB[i]);
        strcpy(BAT[i], TAB[i]);
        reverse(i);
        if (compare(TAB[i], BAT[i]) == 0) {
            printf("%s==%s\n", TAB[i], BAT[i]);
        } else {
            printf("%s!=%s\n", TAB[i], BAT[i]);
        }
    }
}

void reverse(int i) {
    int start = 0, end = strlen(BAT[i]) - 1;
    int temp;
    while (start < end) {
        temp = BAT[i][start];
        BAT[i][start] = BAT[i][end];
        BAT[i][end] = temp;
        start++;
        end--;
    }
}

int compare(char A[10], char B[10]) {
    if (strlen(A) == strlen(B)) {
        for (int i = 0; i < strlen(A); i++) {
            if (A[i] != B[i]) {
                return 1;
            }
        }
        return 0;
    } else {
        return 1;
    }
}
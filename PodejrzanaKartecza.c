#include <stdio.h>
#include <string.h>

int main()
{
	int n;
	char tab[10][30];
	scanf("%d ", &n);
	for(int i = 0; i < n; i++)
	{
		gets(tab[i]);
	}
	int characters=0, letters=0, numbers=0;
	for(int i = 0; i < n; i++)
	{
		characters=0;
		letters=0;
		numbers=0;
		while(tab[i][characters]!=NULL)
		{
			if((tab[i][characters]>47)&&(tab[i][characters]<58))
			{
				numbers++;
			}
			else if(((tab[i][characters]>64)&&(tab[i][characters]<91))||((tab[i][characters]>96)&&(tab[i][characters]<123)))
			{
				letters++;
			}
			characters++;
		}
		printf("%d %d %d\n", characters, letters, numbers);
	}
}
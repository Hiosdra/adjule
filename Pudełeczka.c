#include <stdio.h>

int main() {
    int n, i, j;
    scanf("%d", &n);
    int box[n];
    for (i = 0; i < n; ++i) {
        scanf("%d", &box[i]);
    }
    int m;
    scanf("%d", &m);
    for (i = 0; i < m; ++i) {
        int k, l;
        scanf("%d %d", &k, &l);
        int result = 0;
        for (j = k-1; j < l; ++j) {
            result += box[j];
        }
        printf("%d\n", result);
    }
    return 0;
}
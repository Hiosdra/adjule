#include <stdio.h>
#include <stdlib.h>

#define DEBUG 0
#define DEBUG1 0
#define DEBUG2 0
#define DEBUG3 0
#define DEBUG0 DEBUG

int compare_ints(const void *p, const void *q);

int main() {
    int i, j, n, m;
    scanf("%d", &n);
    for (i = 0; i < n; ++i) {
        scanf("%d", &m);
        int table[m];
        for (j = 0; j < m; ++j) {
            scanf("%d", &table[j]);
        }
        qsort(&table[0], m, sizeof(table[0]), compare_ints);
        for (j = 0; j < m; ++j) {
            printf("%d", table[j]);
            if(j < m-1) {
                printf(" ");
            }
        }
        printf("\n");
    }
    return 0;
}

int compare_ints(const void *p, const void *q) {
    int x = *(const int *)p;
    int y = *(const int *)q;

    if (x < y)
        return 1;  // Return -1 if you want ascending, 1 if you want descending order.
    else if (x > y)
        return -1;   // Return 1 if you want ascending, -1 if you want descending order.
    return 0;
}
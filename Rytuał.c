#include <stdio.h>
#include <math.h>

int main() {
    int x;
    scanf("%d", &x);
    while(x != 0) {
        int flowers = 0, leaves = 0, counter = 0;
        while(x != 1 && counter < 15) {
            ++counter;
            if (x % 2 == 0) {
                ++flowers;
                x /= 2;
            } else {
                ++leaves;
                x = 3*x+1;
            }
        }
        if(x == 1) {
            printf("TAK %d %d\n", flowers, leaves);
        } else {
            printf("NIE\n");
        }
        scanf("%d", &x);
    }
    return 0;
}


int main()
{
    int x, y, result;
    scanf("%d %d", &x, &y);
    scanf("%d", &result);
    if (result < x) {
        printf("%d", x - result);
    } else if (result > y) {
        printf("%d", result - y);
    } else {
        printf("%s", "BINGO");
    }
    return 0;
}
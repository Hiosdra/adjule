#include <stdio.h>
#include <stdlib.h>

#define DEBUG 0
#define DEBUG1 0
#define DEBUG2 0
#define DEBUG3 0
#define DEBUG0 DEBUG

int factorial(int n, int k);

int main() {
    int i, n;
    scanf("%d", &n);
    for (i = 0; i < n; ++i) {
        int a, b;
        scanf("%d %d", &a, &b);
        printf("%d\n", factorial(a, b));
    }
    return 0;
}

int factorial(int n, int k) {
    if (n < k) return 1;
    if (n >= k) return n * factorial(n - k, k);
    return 0;
}
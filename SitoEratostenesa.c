#include <stdio.h>
#include <math.h>

int isPrime(long potentialPrimeNumber) {
    if(potentialPrimeNumber == 2) {
        return 0;
    }
    if (potentialPrimeNumber % 2 == 0 ||
        potentialPrimeNumber <= 1) {
        return 1;
    }
    int i = 3;
    double potentialPrimeSqrt = sqrt(potentialPrimeNumber);
    while (i < potentialPrimeSqrt) {
        if (potentialPrimeNumber % i == 0) {
            return 1;
        }
        i += 2;
    }
    return 0;
}

int main() {
    int i, n;
    scanf("%d", &n);
    for (i = 0; i < n; ++i) {
        int x;
        scanf("%d", &x);
        if(isPrime(x)) {
            printf("N\n");
        } else {
            printf("T\n");
        }
    }
    return 0;
}


#include <stdio.h>
#include <string.h>

#define DEBUG 0
#define DEBUG1 0
#define DEBUG2 0
#define DEBUG3 0
#define DEBUG0 DEBUG


int main() {
    int i, n;
    scanf("%d", &n);
    char authors[n][30];
    for (i = 0; i < n; ++i) {
        scanf("%s", &authors[i]);
    }
    char spammer[30];
    scanf("%s", &spammer);

    int counter;
    counter = 0;
    for (i = 0; i < n; ++i) {
        if (strcmp(&authors[i], &spammer) == 0) {
            ++counter;
        }
    }
    printf("%d", counter);
    return 0;
}
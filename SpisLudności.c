#include <stdio.h>

struct Person {
    char name[100];
    char surname[100];
    int age;
    float height;
};

int main() {
    int i, n;
    scanf("%d", &n);
    struct Person people[n];
    for (i = 0; i < n; ++i) {
        scanf("%s %s %d %f", &people[i].name, &people[i].surname, &people[i].age, &people[i].height);
    }
    int age;
    float height;
    scanf("%d", &age);
    scanf("%f", &height);
    for (i = 0; i < n; ++i) {
        if (people[i].age > age) {
            printf("%s %s\n", &people[i].name, &people[i].surname);
        }
    }
    printf("%s\n", "----");
    for (i = 0; i < n; ++i) {
        if (people[i].height < height) {
            printf("%s %s\n", &people[i].name, &people[i].surname);
        }
    }
    return 0;
}
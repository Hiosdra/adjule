#include<stdio.h>

int main() {
    int t, i, j, x, s, p;
    scanf("%d", &t);
    for (i = 0; i < t; i++) {
        scanf("%d", &x);
        p = 7;
        s = 7;
        for (j = 1; j < x; j++) {
            s = j % 2 == 0 ? s * 3 : s * 5;
            p += s;
        }
        printf("%d\n", p);
    }
    return 0;
}
#include <iostream>
#include <cstdlib>
#include <string>

void merge(std::string toSort[], std::string helper[], int l, int m, int r);
void mergeSort(std::string toSort[], std::string helper[], int p, int k);

int main() {
	int n;
	std::cin >> n;
	std::string *words = new std::string[n];
	for(int i = 0; i < n; i++) {
		std::cin >> words[i];
	}
	std::string *pom = new std::string[n];
	mergeSort(words, pom, 0, n - 1);
	for(int i = 0; i < n; i++) {
		std::cout << words[i] << std::endl;
	}
	return 0;
}

void merge(std::string toSort[], std::string helper[], int l, int m, int r) {
	int i = l,
        j = m,
        k = l;
    while(i < m && j <= r) {
        if(toSort[i] <= toSort[j]) {
            helper[k++] = toSort[i++];
        } else {
            helper[k++] = toSort[j++];
        }
    }
    while(i < m) {
        helper[k++] = toSort[i++];
    }
    while(j <= r) {
        helper[k++] = toSort[j++];
    }
    for(i = l; i <= r; i++) {
        toSort[i] = helper[i];
    }
}

void mergeSort(std::string toSort[], std::string helper[], int p, int k) {
    int inversions = 0;
    if(k > p) {
        int q = (k + p) / 2;
        mergeSort(toSort, helper, p, q);
        mergeSort(toSort, helper, q + 1, k);
        merge(toSort, helper, p, q + 1, k);
    }
}
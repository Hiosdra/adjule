#include <stdio.h>
#include <math.h>

int main() {
    const double PI = 3.14159265;
    int i;
    float a, b, c, sin;
    int n;
    scanf("%d", &n);
    for (i = 0; i < n; ++i) {
        scanf("%f %f", &a, &b);
        c = sqrt(a * a + b * b);
        if (a > b) {
            sin = b / c;
        } else {
            sin = a / c;
        }
        if (c > round(c)) {
            c = round(c) + 1;
        }
        printf("%.f %.f \n", c, round(asin(sin) * (180 / PI)));
    }
}
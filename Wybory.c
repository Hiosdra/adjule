#include <stdio.h>

int main() {
    int i, numberOfCandidates, votes;
    scanf("%d %d", &numberOfCandidates, &votes);
    int candidates[numberOfCandidates];
    for (i = 0; i < numberOfCandidates; ++i) {
        candidates[i] = 0;
    }
    for (i = 0; i < votes; ++i) {
        int vote;
        scanf(" %d", &vote);
        vote -= 1;
        candidates[vote] += 1;
    }
    int winningCandidateNumber = 0, maxVotes = 0;
    for (i = 0; i < numberOfCandidates; ++i) {
        printf("%d: %d\n", i+1, candidates[i]);
        if(candidates[i] > maxVotes) {
            winningCandidateNumber = i;
            maxVotes = candidates[i];
        }
    }
    printf("%d", winningCandidateNumber + 1);
    return 0;
}
#include <stdio.h>

int main() {
    int i, n;
    scanf("%d", &n);
    char names[n][100];
    char education[n];
    for (i = 0; i < n; i++) {
        scanf("%s %c", &names[i], &education[i]);
    }
    char minEducation;
    scanf(" %c", &minEducation);
    for (i = 0; i < n; ++i) {
        switch (minEducation) {
            case 'p':
                printf("%s\n", names[i]);
                break;
            case 'g':
                if(education[i] != 'p') {
                    printf("%s\n", names[i]);
                }
                break;
            case 's':
                if(education[i] == 's' || education[i] == 'w') {
                    printf("%s\n", names[i]);
                }
                break;
            case 'w':
                if (education[i] == 'w') {
                    printf("%s\n", names[i]);
                }
                break;
        }
    }
    return 0;
}
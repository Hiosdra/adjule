int main()
{
    int n;
    scanf("%d", &n);
    int numbers[n];
    int i;
    for (i = 0; i < n; ++i) {
        scanf("%d", &numbers[i]);
    }
    for (i = n; i > 0; --i) {
        if(i % 2 == 0) {
            printf("%d ", numbers[i -1]);
        }
    }
    return 0;
}
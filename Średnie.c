#include <stdio.h>
#include <math.h>
#include <malloc.h>

typedef struct node Node;

typedef struct list List;

List * makelist();
void add(int data, List * list);
void display(List * list);
float avg(List * list);

int main() {
    int scannedNumber;
    List* list = makelist();
    scanf("%d", &scannedNumber);
    while(scannedNumber != -1) {
        switch(scannedNumber) {
            case 1:
                printf("%.2f\n", avg(list));
                break;
            case 0:
                display(list);
                break;
            default:
                add(scannedNumber, list);
        }
        scanf("%d", &scannedNumber);
    }

    return 0;
}

struct node {
    int data;
    struct node * next;
};

struct list {
    Node * head;
};

Node * createnode(int data);

Node * createnode(int data){
    Node * newNode = malloc(sizeof(Node));
    if (!newNode) {
        return NULL;
    }
    newNode->data = data;
    newNode->next = NULL;
    return newNode;
}

List * makelist(){
    List * list = malloc(sizeof(List));
    if (!list) {
        return NULL;
    }
    list->head = NULL;
    return list;
}

void display(List * list) {
    Node * current = list->head;
    if(list->head == NULL)
        return;

    for(; current != NULL; current = current->next) {
        printf("%d ", current->data);
    }
    printf("%s", "\n");
}

float avg(List * list) {
    float sum = 0, num = 0;
    Node * current = list->head;
    if(list->head == NULL)
        return -1;

    for(; current != NULL; current = current->next) {
        sum += current->data;
        ++num;
    }
    return sum / num;
}

void add(int data, List * list){
    Node * current = NULL;
    if(list->head == NULL){
        list->head = createnode(data);
    }
    else {
        current = list->head;
        while (current->next!=NULL){
            current = current->next;
        }
        current->next = createnode(data);
    }
}